import {Form as VeeForm, Field as VeeField} from 'vee-validate';
export default {
    install(app){
        app.component('vee-from',VeeForm);
        app.component('vee-field',VeeField);
    },
}
import { createApp } from 'vue'
import App from './App.vue'
import VeevalidateSettings from './includes/veevalidation';
createApp(App)
.use(VeevalidateSettings)
.mount('#app');

